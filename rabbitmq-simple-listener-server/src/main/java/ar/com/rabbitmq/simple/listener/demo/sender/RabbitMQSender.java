package ar.com.rabbitmq.simple.listener.demo.sender;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * RabbitMQ Sender class.
 */
@Service
public class RabbitMQSender {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    @Value("${rabbitmq.exchange}")
    private String exchange;

    @Value("${rabbitmq.routingkey}")
    private String routingkey;

    /**
     * @param message {@link String}
     * @throws AmqpException throws {@link AmqpException} to controller.
     */
    public void send(final String message) throws AmqpException {
        try {
            rabbitTemplate.convertAndSend(exchange, routingkey, message);
        } catch (AmqpException e) {
            throw new AmqpException(e);
        }
        System.out.println("Send msg = " + message);

    }
}