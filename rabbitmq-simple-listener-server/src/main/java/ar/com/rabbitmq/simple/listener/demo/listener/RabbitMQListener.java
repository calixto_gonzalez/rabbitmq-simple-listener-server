package ar.com.rabbitmq.simple.listener.demo.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class RabbitMQListener {


    private static final Logger log = LoggerFactory.getLogger(RabbitListener.class);

    /**
     *
     * @param message {@link String}
     */
    @RabbitListener(queues = "${rabbitmq.queue}")
    public void receiveMessageFromQueue(final String message) {
        log.info("Received message: {} from hola.queue.",message);


    }


}
