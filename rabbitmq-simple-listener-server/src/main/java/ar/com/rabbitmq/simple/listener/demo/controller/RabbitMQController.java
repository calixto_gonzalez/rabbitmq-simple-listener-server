package ar.com.rabbitmq.simple.listener.demo.controller;

import ar.com.rabbitmq.simple.listener.demo.sender.RabbitMQSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * RabbitMQ controller mediates between http requests and messages.
 */
@RestController
@RequestMapping(value = "/message")
public class RabbitMQController {

    private RabbitMQSender rabbitMQSender;
    private static final Logger log = LoggerFactory.getLogger(RabbitMQController.class);

    /**
     *
     * @param rabbitMQSender {@link RabbitMQSender}
     */
    @Autowired
    public RabbitMQController(final RabbitMQSender rabbitMQSender) {
        this.rabbitMQSender=rabbitMQSender;
    }

    /**
     *
     * @param message {@link String} message to send
     * @return ResponseEntity {@link ResponseEntity} with the http status and it's message
     */
    @RequestMapping(method = RequestMethod.POST,consumes = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity sender(@RequestBody final String message) {

        log.debug("Message about to be sent {}", message);

        try {
            rabbitMQSender.send(message);
            return new ResponseEntity("Message sent.",HttpStatus.ACCEPTED);
        } catch (AmqpException e) {
            return new ResponseEntity(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

}
