package ar.com.rabbitmq.simple.listener.demo.listener.config;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration bean for RabbitMQ , sets {@link ConnectionFactory},{@link Queue},{@link org.springframework.amqp.core.Exchange},{@link MessageConverter} and others.
 */
@Configuration
public class RabbitMQConfig {
    @Value("${rabbitmq.queue}")
    private String queueName;

    @Value("${rabbitmq.exchange}")
    private String exchange;

    @Value("${rabbitmq.routingkey}")
    private String routingkey;

    /**
     * @return Queue {@link Queue} settings of the queue, go here.
     * for reference <a href="https://docs.spring.io/spring-amqp/docs/current/api/org/springframework/amqp/core/Queue.html">Spring Documentation {@link Queue}</a>
     */
    @Bean
    Queue queue() {
        return new Queue(queueName, false);
    }

    /**
     * @return DirectExchange {@link DirectExchange}
     * <p>
     * for reference <a href="https://docs.spring.io/spring-amqp/docs/current/api/org/springframework/amqp/core/DirectExchange.html">Spring Documentation {@link DirectExchange}</a>
     */
    @Bean
    DirectExchange exchange() {
        return new DirectExchange(exchange);
    }

    /**
     * @param queue    {@link Queue}
     * @param exchange {@link org.springframework.amqp.core.Exchange}
     * @return Binding {@link Binding} binds queue to exchange with the designated routing key.
     * <p>
     * for reference <a href="https://docs.spring.io/spring-amqp/docs/current/api/org/springframework/amqp/core/Binding.html">Spring Documentation {@link Binding}</a>
     */
    @Bean
    Binding binding(final Queue queue, final DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(routingkey);
    }

    /**
     * TODO research if this is necessary.
     *
     * @return MessageConverter {@link MessageConverter}
     * <p>
     * for reference  <a href="https://docs.spring.io/spring-amqp/docs/current/api/org/springframework/amqp/support/converter/MessageConverter.html">Spring Documentation {@link MessageConverter}</a>
     */
    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    /**
     * @param connectionFactory {@link ConnectionFactory}
     * @return AmqpTemplate {@link AmqpTemplate} basically the connection to the RabbitMQ broker.
     * <p>
     * for reference <a href="https://docs.spring.io/spring-amqp/docs/current/api/org/springframework/amqp/core/AmqpTemplate.html">Spring Documentation {@link AmqpTemplate}</a>
     */
    @Bean
    public AmqpTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }


}
